package com.zsta.test;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: zhangshaotao
 * Date: 2020-02-29
 * Time: 16:29
 * To change this template use File | Settings | File Templates.
 */
public class ZipUtil {

	public static void zipMultiFile(String filepath ,ZipOutputStream zipOutputStream, boolean dirFlag) {
		try {
			File file = new File(filepath);// 要被压缩的文件夹
//			ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFile));
			ZipOutputStream zipOut = zipOutputStream;
			if(file.isDirectory()){
				File[] files = file.listFiles();
				for(File fileSec:files){
					if(dirFlag){
						recursionZip(zipOut, fileSec, file.getName() + File.separator);
					}else{
						recursionZip(zipOut, fileSec, "");
					}
				}
			}
//			zipOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void recursionZip(ZipOutputStream zipOut, File file, String baseDir) throws Exception{
		if(file.isDirectory()){
			File[] files = file.listFiles();
			for(File fileSec:files){
				recursionZip(zipOut, fileSec, baseDir + file.getName() + File.separator);
			}
		}else{
			byte[] buf = new byte[1024];
			InputStream input = new FileInputStream(file);
			zipOut.putNextEntry(new ZipEntry(baseDir + file.getName()));
			int len;
			while((len = input.read(buf)) != -1){
				zipOut.write(buf, 0, len);
			}
			input.close();
		}
	}

	public static void main(String[] args) throws IOException {
		File zipFile = new File("/tmp/zip/tmp.zip");
		ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
		zipMultiFile("/tmp/zip/test", zipOutputStream, Boolean.TRUE);
		zipMultiFile("/tmp/zip/data", zipOutputStream, Boolean.TRUE);
		zipMultiFile("/tmp/zip/mozo", zipOutputStream, Boolean.TRUE);
		zipOutputStream.flush();
		zipOutputStream.close();
	}
}
