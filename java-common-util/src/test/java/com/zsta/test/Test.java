package com.zsta.test;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: zhangshaotao
 * Date: 2020-03-27
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
public class Test {

	// a:  234113454
	// b:  123453453
	String sub(String a, String b) {

		char[] aArr = a.toCharArray();
		char[] bArr = b.toCharArray();

		int aLen = aArr.length;
		int bLen = bArr.length;

		int len = aLen > bLen ? aLen : bLen;
		int[] result = new int[len];

		boolean isAMax = aLen > bLen || (aLen == bLen && aArr[0] >= bArr[0]);
		char sign = isAMax ? '+' : '-';
		if (isAMax) {
			for (int i = 0; i < aArr.length; i++  ) {
				int aSubInt = (int) aArr[i] - (int)'0';
				int bSubInt = (int) bArr[i] - (int)'0';
				result[i] = aSubInt - bSubInt;
			}
		} else {
			for (int i = 0; i < aArr.length; i++  ) {
				int aSubInt = (int) aArr[i] - (int)'0';
				int bSubInt = (int) bArr[i] - (int)'0';
				result[i] = bSubInt - aSubInt;
			}
		}

		return sign + result.toString();
	}
}
