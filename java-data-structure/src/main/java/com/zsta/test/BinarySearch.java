package com.zsta.test;

/**
 * Created with IntelliJ IDEA.
 * User: zsta
 * Date: 19-2-24
 * Time: 下午8:29
 * To change this template use File | Settings | File Templates.
 */
public class BinarySearch {

    public static void main(String[] args) {
        int arr[] = {3, 6, 9, 12};
        int s = 12;
//        binarySearch(arr, s);
//        System.out.println(binarySearch2(arr, s));
        binarySearch3(arr, s);
    }

    public static void binarySearch(int[] arr, int g) {
        int arrMidIndex = arr.length % 2 == 0 ? arr.length / 2 : (arr.length + 1) / 2;
        int arrMid = arr[arrMidIndex];

        while (arrMid != g) {
            if (g > arrMid) {
                arrMidIndex = (arr.length - arrMidIndex) % 2 == 0 ?
                        (arr.length - arrMidIndex) / 2
                        : (arr.length - arrMidIndex + 1) / 2 + arrMidIndex;
            }
            if (g < arrMid) {
                arrMidIndex = arrMidIndex % 2 == 0 ? arrMidIndex / 2 : (arrMidIndex + 1) / 2;
            }

            arrMid = arr[arrMidIndex];
        }

        System.out.println(arrMid + "---index:" + arrMidIndex);
    }

    static int binarySearch2(int[] array, int key) {
        int left = 0;
        int right = array.length - 1;

        // 这里必须是 <=
        while (left <= right) {
            int mid = (left + right) / 2;
            if (array[mid] == key) {
                return mid;
            }
            else if (array[mid] < key) {
                left = mid + 1;
            }
            else {
                right = mid - 1;
            }
        }

        return -1;
    }

    public static void binarySearch3(int[] arr, int g) {
        int l = 0, r = arr.length, mid = 0;
        while (l <= r) {
            mid = (l + r) / 2;

            if (g < arr[mid]) {
                r--;
            } else {
                l++;
            }

            if (arr[mid] == g) {
                break;
            }
        }

        System.out.println(mid);
    }
}
