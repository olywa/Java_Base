package com.zsta.test;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-12-25
 * Time: 18:05
 * To change this template use File | Settings | File Templates.
 */
public class BubbleSort {

	public static void main(String[] args) {
		int[] arr = {2, 9, 5, 4, 8, 0, 10, 6, 3};
		BubbleSort bubbleSort = new BubbleSort();
		bubbleSort.bubbleSort(arr);
		for (int i : arr) {
			System.out.println(i);
		}
	}

	public void bubbleSort(int[] arr) {
		for (int i=0; i < arr.length; i++) {
			for (int j=arr.length - 1; j>i; j--) {
				int temp = arr[i];
				if (arr[i] > arr[j]) {
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
}
