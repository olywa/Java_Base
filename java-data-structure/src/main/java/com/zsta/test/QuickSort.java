package com.zsta.test;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-12-25
 * Time: 15:47
 * To change this template use File | Settings | File Templates.
 */
public class QuickSort {

	public static void main(String[] args) {
		QuickSort quickSort = new QuickSort();
		int[] arr = {2, 9, 5, 4, 8, 0, 10, 6, 3};
		quickSort.quickSort(arr, 0, 8);
		for (int i=0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}

		Arrays.sort(arr);
	}

	public void quickSort(int[] arr, int left, int right) {
		if (left < right) {
			int dp = parition(arr, left, right);
			quickSort(arr, left, dp - 1);
			quickSort(arr, dp + 1, right);
		}
	}

	public int parition(int[] a, int left, int right) {

		int i = left;
		int j = right;
		int temp = a[i];
		while (i < j) {
			while (i < j && a[j] >= temp) {
				j--;
			}
			a[i] = a[j];

			while (i < j && a[i] <= temp) {
				i++;
			}
			a[j] = a[i];
		}

		a[i] = temp;
		return i;
	}

	public void quickSort_2(int[] arr, int left, int right) {
		int i = left;
		int j = right;
		int temp = arr[left];
		if (left >= right) {
			return;
		}

		while (i != j ) {
			while (i < j && arr[j] >= temp) {
				j--;
				if (i < j) {
					arr[i] = arr[j];
				}
			}
			while (i < j && arr[i] <= temp) {
				i++;
				if (i < j) {
					arr[j] = arr[i];
				}
			}
		}

		arr[i] = temp;
		quickSort_2(arr, left, i-1);
		quickSort_2(arr, i+1, right);
	}

	public void quickSort_3(int []array,int lo,int hi){

		if(lo>=hi){
			return;
		}

		//固定的切分方式
		int key=array[lo];
		while(lo < hi){
			while(array[hi]>=key&&hi>lo){//从后半部分向前扫描
				hi--;
			}
			array[lo]=array[hi];
			while(array[lo]<=key&&hi>lo){ //从前半部分向后扫描
				lo++;
			}
			array[hi]=array[lo];
		}
		array[hi]=key;

		quickSort_3(array, lo, lo - 1);
		quickSort_3(array, lo + 1, hi);
	}
}
