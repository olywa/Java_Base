package com.zsta.test;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-12-25
 * Time: 17:45
 * To change this template use File | Settings | File Templates.
 */
public class QuickSortV2 {

	public static void main(String[] args) {
		QuickSortV2 quickSort = new QuickSortV2();
		int[] arr = {2, 9, 5, 4, 8, 0, 10, 6, 3};
		quickSort.sort(arr, 0, 8);
		for (int i=0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}

	}

	public int partition(int []array,int lo,int hi){
		//固定的切分方式
		int key=array[lo];
		while(lo<hi){
			while(array[hi]>=key&&hi>lo){//从后半部分向前扫描
				hi--;
			}
			array[lo]=array[hi];
			while(array[lo]<=key&&hi>lo){ //从前半部分向后扫描
				lo++;
			}
			array[hi]=array[lo];
		}
		array[hi]=key;
		return hi;
	}

	public void sort(int[] array,int lo ,int hi){
		if(lo>=hi){
			return ;
		}
		int index=partition(array, lo, hi);
		sort(array,lo,index-1);
		sort(array,index+1,hi);
	}
}
