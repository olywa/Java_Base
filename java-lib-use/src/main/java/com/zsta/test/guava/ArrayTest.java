package com.zsta.test.guava;

import com.google.common.primitives.Ints;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-02-12
 * Time: 15:13
 * To change this template use File | Settings | File Templates.
 */
public class ArrayTest {

	public void arrayContains1() {
		int[] array = { 1, 2, 3, 4, 5 };
		int a = 4;
		boolean hasA = false;
		for (int i : array) {
			if (i == a) {
				hasA = true;
			}
		}
	}

	public void arrayContains2() {
		int[] array = { 1, 2, 3, 4, 5 };
		boolean isContains = Ints.contains(array, 4);
	}

	public void test() {
		int[] array = { 1, 2, 3, 4, 5 };
		int max = Ints.max(array);
		int min = Ints.min(array);
		int index = Ints.indexOf(array, 2);  //求某元素在数组的位置
		int[] arrayConcat = Ints.concat(array, new int[]{5, 6}); //合并
	}

}
