package com.zsta.test.guava;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-02-12
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class MapTest {


	public void test1() {

	}

}

class PersonCardService {

	final Map<Person, List<Card>> cardListMap = Maps.newHashMap();

	final Multimap<Person, Card> cardMap = ArrayListMultimap.create();

	public void addCard1(final Person person, final Card card) {
		List<Card> cardList = cardListMap.get(person);
		if (cardList == null) {
			cardList = Lists.newArrayList();
		}
		cardList.add(card);

		cardListMap.put(person, cardList);
	}

	public void addCard2(final Person person, final Card card) {
		cardMap.put(person, card);
	}

	public void foreachCardMap2() {
		Map<Person, Collection<Card>> listMap = cardMap.asMap();
	}

}

class Person {
	String name;
	int age;
}

class Card {
	String name;
	long cardNum;
}
