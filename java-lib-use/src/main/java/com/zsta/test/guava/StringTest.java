package com.zsta.test.guava;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-02-12
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */

public class StringTest {

	public static void main(String[] args) {
//		CharMatcher.is(‘aaa’), CharMatcher.isNot(‘bbb’), CharMatcher.oneOf(‘abcd’).negate()
		CharMatcher matcher = CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('A', 'Z'));
//		char c = '1';
		char c = 'b';
		System.out.println(matcher.matches(c));

//		System.out.println(CharMatcher.DIGIT.retainFrom("some text 89983 and more"));
//		System.out.println(CharMatcher.DIGIT.removeFrom("some text 89983 and more"));

		System.out.println(CharMatcher.javaDigit().retainFrom("some text 89983 and more"));
		System.out.println(CharMatcher.digit().removeFrom("some text 89983 and more"));
	}


	@Test
	public void joinerTest() {
		String[] subdirs = { "usr", "local", "lib" };
		System.out.println(Joiner.on("/").join(subdirs));

		int[] nums = {2, 5, 7, 0};
		System.out.println(Joiner.on(";").join(Ints.asList(nums)));
		System.out.println(Ints.join(";", nums));

		String numAsString = Ints.join(",", nums);
		String[] numStrings = numAsString.split(",");

		Iterable iterable = Splitter.on(",").split(numAsString);

		Iterable<String> iterable1 = Splitter.on(",")
				.omitEmptyStrings()
				.trimResults()
				.split(numAsString);

		//将规则字符转为map
		String str = "key1: v1, key2: v2, key3: v3";
		Map<String, String> map = Splitter.on(",")
				.trimResults()
				.withKeyValueSeparator(":")
				.split(str);

		ArrayList<String> strArr1 = Lists.newArrayList(
				"test1","test2","test3",null,"test4",null,null);

		//拼接字符--过滤null
		Joiner.on(';')
				.skipNulls()
				.join(strArr1);

		//拼接字符--替代
		Joiner.on(';')
				.useForNull("_")
				.join(strArr1);

		map.put("key4", "v4");
		map.put("key5", "v5");

		String toMapString = Joiner.on(";")
				.useForNull("NULL")
				.withKeyValueSeparator(":")
				.join(map);

		System.out.println(toMapString);

	}
}
