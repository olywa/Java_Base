package com.zsta.test.poi;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-07-12
 * Time: 15:25
 * To change this template use File | Settings | File Templates.
 */
public class PoiTest {
	private static final String EXCEL_XLS = "xls";
	private static final String EXCEL_XLSX = "xlsx";


	public static void main(String[] args) throws IOException {
		File file = new File("/home/zsta/Desktop/zst/data/xls/样本数据_BOM.xls");
		InputStream inputStream = new FileInputStream(file);
		Workbook workbook = getWorkbok(inputStream, file);

	}

	public static Workbook getWorkbok(InputStream in, File file) throws IOException {
		Workbook wb = null;
		if(file.getName().endsWith(EXCEL_XLS)){  //Excel 2003
			wb = new HSSFWorkbook(in);
//			wb = new XSSFWorkbook(in);
		}else if(file.getName().endsWith(EXCEL_XLSX)){  // Excel 2007/2010
			wb = new XSSFWorkbook(in);
		}
		return wb;
	}

	/**
	 * 判断文件是否是excel
	 * @throws Exception
	 */
	public static void checkExcelVaild(File file) throws Exception {
		if (!file.exists()) {
			throw new Exception("文件不存在");
		}
		if (!(file.isFile() && (file.getName().endsWith(EXCEL_XLS) || file.getName().endsWith(EXCEL_XLSX)))) {
			throw new Exception("文件不是Excel");
		}
	}
}
