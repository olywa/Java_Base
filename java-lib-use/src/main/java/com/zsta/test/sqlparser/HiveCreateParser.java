package com.zsta.test.sqlparser;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.parser.SimpleNode;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.StatementVisitor;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.util.TablesNamesFinder;
import net.sf.jsqlparser.util.deparser.StatementDeParser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-02-28
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
public class HiveCreateParser {

	public static void main(String[] args) throws JSQLParserException {
//		String sql = "select t1.name as dbName, t2. from DBS t1 left join hive_meta_buss.hive_meta_db_detail t2 on t1.name = t2.db_name where t2.is_show=1";
		String sql = "CREATE TABLE 库名.表名 " +
		"(" +
				"col1 bigint comment '字段注释1'" +
				",col2 string comment '字段注释2'" +
				",col3 boolean comment '字段注释3'" +
				",col4 timestamp comment '字段注释4'" +
				",col5 decimal comment '字段注释5'" +
				",col6 array(string) comment '字段注释6'" +
				") comment '表注释'" +
				"PARTITIONED BY (dt string comment '分区字段注释')";
//		String sql = "CREATE TABLE 库名.表名 " +
//		"(" +
//				"col1 bigint comment '字段注释1'" +
//				",col2 string comment '字段注释2'" +
//				",col3 boolean comment '字段注释3'" +
//				",col4 timestamp comment '字段注释4'" +
//				",col5 decimal comment '字段注释5'" +
//				",col6 array(string) comment '字段注释6'" +
//				") comment '表注释'" +
//				"PARTITIONED BY (dt string comment '分区字段注释')";
//				"STORED AS orcfile;";
//		String sql = "CREATE TABLE 库名.表名 " +
//		"(" +
//				"col1 bigint " +
//				",col2 string " +
//				",col3 boolean " +
//				",col4 timestamp " +
//				",col5 decimal " +
//				",col6 array(string) " +
//				") " +
//				"PARTITIONED BY (dt string )" ;
//				"STORED as orcfile;";
//		String sql = "CREATE TABLE etl_dw_export_log (\n" +
//				"\n" +
//				"task_name varchar(100) DEFAULT NULL \n" +
//				"dbtype varchar(100) DEFAULT NULL \n" +
//				"tdbname varchar(100) DEFAULT NULL \n" +
//				"tname varchar(100) DEFAULT NULL \n" +
//				"err_msg text \n" +
//				") ENGINE=InnoDB DEFAULT CHARSET=utf8 ;";
		System.out.println(sql);
//		System.out.println(HiveCreateParser.parserHiveCreateSql(sql));
//		HiveCreateParser.getColumnByCreateSql(HiveCreateParser.parserHiveCreateSql(sql));
	}

	public static String parserHiveCreateSql(String sql) {
		return sql.replaceAll("comment", "");
	}

	public static List<String> getTableNameBySql(String sql) throws JSQLParserException {
		CCJSqlParserManager parser=new CCJSqlParserManager();
		StringReader reader=new StringReader(sql);
		List<String> list=new ArrayList<String>();
		Statement stmt=parser.parse(new StringReader(sql));
		if (stmt instanceof Select) {
			Select selectStatement = (Select) stmt;
			TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
			List tableList = tablesNamesFinder.getTableList(selectStatement);
			for (Iterator iter = tableList.iterator(); iter.hasNext();) {
				String tableName=iter.next().toString();
				list.add(tableName);
				System.out.println(tableName);
			}

//			List<String> column = selectStatement
		}
		return list;
	}

	public static List<String> getColumnByCreateSql(String sql) throws JSQLParserException {
//		CCJSqlParserManager parser=new CCJSqlParserManager();
		List<String> list=new ArrayList<String>();
//		Statement stmt=parser.parse(new StringReader(sql));
		Statement stmt = CCJSqlParserUtil.parse(new StringReader(sql));
		if (stmt instanceof CreateTable) {
			CreateTable createTable = (CreateTable) stmt;
			TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
			List tableList = tablesNamesFinder.getTableList(createTable);
			for (Iterator iter = tableList.iterator(); iter.hasNext();) {
				String tableName=iter.next().toString();
				list.add(tableName);
				System.out.println(tableName);
			}

			List<ColumnDefinition> columnDefinitionList = createTable.getColumnDefinitions();
			for (ColumnDefinition columnDefinition : columnDefinitionList) {
				System.out.println(columnDefinition.getColumnName() + "--" + columnDefinition.getColDataType());
			}

//			PlainSelect selectBody = (PlainSelect) createTable.getSelectBody();
//			List<SelectItem> selectList = selectBody.getSelectItems();
//			for (SelectItem item : selectList) {
//				SimpleNode node = item.getASTNode();
//				SelectExpressionItem selectItem = (SelectExpressionItem)item;
//				System.out.println(selectItem.getExpression() + "------"+ selectItem.getAlias());
//			}
		}
		return list;
	}

}
