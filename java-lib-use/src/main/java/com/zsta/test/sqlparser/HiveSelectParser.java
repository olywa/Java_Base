package com.zsta.test.sqlparser;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.SimpleNode;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.util.TablesNamesFinder;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-02-28
 * Time: 15:17
 * To change this template use File | Settings | File Templates.
 */
public class HiveSelectParser {
	public static void main(String[] args) throws JSQLParserException {
//		String sql = "select t1.name as dbName, t2. from DBS t1 left join hive_meta_buss.hive_meta_db_detail t2 on t1.name = t2.db_name where t2.is_show=1";
		String sql = "select task_name as taskName, total_time as totalTime, average_flow as averageFlow, sum_line as sumLine, err_line as errLine, err_msg as errMsg," +
				"write_speed as wirteSpeed, date_format(start_time, '%Y-%m-%d %H:%i:%s') as startTime, date_format(end_time, '%Y-%m-%d %H:%i:%s') as endTime " +
				"from etl_dw_export_log where str_to_date(start_time, '%Y-%m-%d')=? and task_name = ? order by start_time";
		HiveSelectParser.getTableNameBySql(sql);
	}

	public static List<String> getTableNameBySql(String sql) throws JSQLParserException {
		CCJSqlParserManager parser=new CCJSqlParserManager();
		StringReader reader=new StringReader(sql);
		List<String> list=new ArrayList<String>();
		Statement stmt=parser.parse(new StringReader(sql));
		if (stmt instanceof Select) {
			Select selectStatement = (Select) stmt;
			TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
			List tableList = tablesNamesFinder.getTableList(selectStatement);
			for (Iterator iter = tableList.iterator(); iter.hasNext();) {
				String tableName=iter.next().toString();
				list.add(tableName);
				System.out.println(tableName);
			}

			PlainSelect selectBody = (PlainSelect) selectStatement.getSelectBody();
			List<SelectItem> selectList = selectBody.getSelectItems();
			for (SelectItem item : selectList) {
				SimpleNode node = item.getASTNode();
				SelectExpressionItem selectItem = (SelectExpressionItem)item;
				System.out.println(selectItem.getExpression() + "------"+ selectItem.getAlias());
			}
		}
		return list;
	}
}
