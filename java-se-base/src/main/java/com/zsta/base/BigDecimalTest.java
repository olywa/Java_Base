package com.zsta.base;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created with IntelliJ IDEA.
 * User: zhangshaotao
 * Date: 2020-09-04
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */
public class BigDecimalTest {

	public static void main(String[] args) {
		BigDecimal a = new BigDecimal(34);
		BigDecimal b = new BigDecimal(35);

		System.out.println(a.add(b));
		System.out.println(a.subtract(b));
		System.out.println(a.multiply(b));
//		System.out.println(a.divide(b));  java.lang.ArithmeticException
		System.out.println(a.divide(b, 5, BigDecimal.ROUND_HALF_UP));

//		ROUND_CEILING    //向正无穷方向舍入
//		ROUND_DOWN    //向零方向舍入
//		ROUND_FLOOR    //向负无穷方向舍入
//		ROUND_HALF_DOWN    //向（距离）最近的一边舍入，除非两边（的距离）是相等,如果是这样，向下舍入, 例如1.55 保留一位小数结果为1.5
//		ROUND_HALF_EVEN    //向（距离）最近的一边舍入，除非两边（的距离）是相等,如果是这样，如果保留位数是奇数，使用ROUND_HALF_UP，如果是偶数，使用ROUND_HALF_DOWN
//		ROUND_HALF_UP    //向（距离）最近的一边舍入，除非两边（的距离）是相等,如果是这样，向上舍入, 1.55保留一位小数结果为1.6,也就是我们常说的“四舍五入”
//		ROUND_UNNECESSARY    //计算结果是精确的，不需要舍入模式
//		ROUND_UP    //向远离0的方向舍入

		BigDecimal c = new BigDecimal("2.4352324");
		System.out.println(c.setScale(4, RoundingMode.HALF_UP));  //四舍五入保留4位
	}
}
