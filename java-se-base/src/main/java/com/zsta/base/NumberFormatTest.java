package com.zsta.base;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created with IntelliJ IDEA.
 * User: zhangshaotao
 * Date: 2020-09-04
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
public class NumberFormatTest {

	public static void main(String[] args) {
		BigDecimal bigDecimal = new BigDecimal(34).divide(new BigDecimal(35), 4, BigDecimal.ROUND_HALF_UP);

		System.out.println(bigDecimal.multiply(new BigDecimal(100)) + "%");

		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(4);
		String reslut = numberFormat.format((float) 34 / (float) 35 * 100);
		System.out.println(reslut + "%");

		NumberFormat numberFormat1 = NumberFormat.getPercentInstance();
		DecimalFormat percentFormat = (DecimalFormat) numberFormat1;

//		percentFormat.applyPattern("##.00");
		//设置小数部分 最小位数为2
		percentFormat.setMinimumFractionDigits(2);
		System.out.println(percentFormat.format(0.912345));

		percentFormat.setMinimumFractionDigits(2);
		System.out.println(numberFormat1.format((float) 34 / (float) 35));
	}
}
