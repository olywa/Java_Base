package com.zsta.test.thread;


import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * User: zsta
 * Date: 19-2-24
 * Time: 下午9:13
 * To change this template use File | Settings | File Templates.
 *
 * 多线程交替执行
 *
 *  Printer1:  实现两个线程交替打印0-20git
 *  Printer4:  实现三个线程交替打印
 *
 */
public class MultiThreadRunInTurn {

//    private static volatile int i = 0;
    private static int i = 0;

    public static void main(String[] args) {
        Object lock = new Object();

//        new Thread(new Printer1(lock)).start();
//        new Thread(new Printer1(lock)).start();

//        new Thread(new Printer3()).start();
//        new Thread(new Printer3()).start();


        Printer4 printer4 = new Printer4();

        new Thread(new Runnable() {
            @Override
            public void run() {
                printer4.task1();
            }
        }).start();

        new Thread(() -> printer4.task2()).start();
        new Thread(() -> printer4.task3()).start();


    }

    /**
     *
     * 使用多线程争用一把锁，并切换状态:等待 唤醒
     *
     * */
    static class Printer1 implements Runnable {

//        public volatile int i = 0;
        private Object lock;

        public Printer1(Object lock) {
            this.lock = lock;
        }

        @Override
        public void run() {
            try {

                synchronized (lock) {
                    while (i <= 20) {
                        System.out.println(Thread.currentThread().getName() + "---" + i++);
//                        Thread.sleep(1000);
                        lock.notify();
                        lock.wait();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    static class Printer2 implements Runnable {
//
////        public volatile int i = 0;
//        private Object lock;
//
//        public Printer2(Object lock) {
//            this.lock = lock;
//        }
//
//        @Override
//        public void run() {
//            try {
//
//                synchronized (lock) {
//                    while (i <= 20 ) {
//                        System.out.println(Thread.currentThread().getName() + "---" + i++);
////                        Thread.sleep(1000);
//                        lock.notify();
//                        lock.wait();
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }

    /**
     *
     *  没用使用同一个锁
     *
     * */
    static class Printer3 implements Runnable {

        @Override
        public synchronized void run() {
            try {

                while (i <= 20 ) {
                    System.out.println(Thread.currentThread().getName() + "---" + i++);
//                        Thread.sleep(1000);
                    wait();
                    notify();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    static class Printer4 {

        ReentrantLock lock = new ReentrantLock();

        Condition con1 = lock.newCondition();
        Condition con2 = lock.newCondition();
        Condition con3 = lock.newCondition();

        int flag = 0;

        void task1() {
            lock.lock();
            try {

                while (i <= 30) {
                    if (flag != 0)
                        con1.await();
                    System.out.println(Thread.currentThread().getName() + "---" + i++);
                    flag = 1;
                    con2.signal();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        void task2() {
            lock.lock();
            try {

                while (i <= 30) {
                    if (flag != 1)
                        con2.await();
                    System.out.println(Thread.currentThread().getName() + "---" + i++);
                    flag = 2;
                    con3.signal();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        void task3() {
            lock.lock();
            try {

                while (i <= 30) {
                    if (flag != 2)
                        con3.await();
                    System.out.println(Thread.currentThread().getName() + "---" + i++);
                    flag = 0;
                    con1.signal();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
