package com.zsta.test.thread.interrupt;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-11
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class ThreadInterrupt {
	public static void main(String[] args) throws InterruptedException {
		Thread worker = new Thread(new Interrupt());
		worker.start();
		System.out.println("worker: " + worker.isInterrupted());
		System.out.println("worker: " + worker.isInterrupted());
		worker.interrupt();
		//参看 https://www.jianshu.com/p/47cc0c0aa4a3
		System.out.println("worker: " + worker.interrupted());
		System.out.println("worker: " + worker.interrupted());

		Thread.sleep(5000);
		// 作用是测试【当前】线程是否被中断（检查中断标志），返回一个boolean并清除中断状态，第二次再调用时中断状态已经被清除，将返回一个false。
		Thread.currentThread().interrupt();
		System.out.println("main: " + Thread.currentThread().interrupted());
		System.out.println("main: " + Thread.currentThread().interrupted());
	}
}

class Interrupt implements Runnable {

/*	@Override
	public void run() {
		try {
			System.out.println("task running...");
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}*/

	@Override
	public void run() {
		while (true) {
			System.out.println("task running: " + Thread.currentThread().interrupted());
			System.out.println("task running: " + Thread.currentThread().interrupted());
			long time = System.currentTimeMillis();
			while (System.currentTimeMillis() - time < 100) {}
//			try {
//				Thread.sleep(100);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//				break;  //return  可通过此种方式，结束线程
//			}
//			boolean isInterrupt = Thread.currentThread().isInterrupted();
//			if (isInterrupt) {
//
//			}
		}
	}
}
