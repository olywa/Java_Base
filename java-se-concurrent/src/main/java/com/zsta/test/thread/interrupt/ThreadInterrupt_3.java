package com.zsta.test.thread.interrupt;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-11
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class ThreadInterrupt_3 extends Thread{

	public static void main(String[] args) throws InterruptedException {
		ThreadInterrupt_3 worker = new ThreadInterrupt_3();
		worker.start();
		Thread.sleep(3000);
		System.out.println("task to be stop");
		worker.interrupt();
		Thread.sleep(1000);
		System.out.println("task stop");
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			long time = System.currentTimeMillis();
			System.out.println("task running...");
			/*
             * 使用while循环模拟 sleep 方法，这里不要使用sleep，否则在阻塞时会 抛
             * InterruptedException异常而退出循环，这样while检测stop条件就不会执行，
             * 失去了意义。
             */
			while(System.currentTimeMillis() - time < 1000) {}
		}
	}
}

