package com.zsta.test.thread.interrupt;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-11
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 *
 * 实现强制结束结束进程的功能（类似stop（））
 *
 */
public class ThreadService {

	private Thread executeThread;

	private boolean finished = false;

	public void execute(Runnable task) {
		executeThread = new Thread(() -> {
			Thread runner = new Thread(task);
			runner.setDaemon(true);

			runner.start();
			try {
				runner.join();
				finished = true;
			} catch (InterruptedException e) {
//				e.printStackTrace();
			}
		});

		executeThread.start();
	}

	public void shutdown(long mills) {
		long time = System.currentTimeMillis();
		while (!finished) {
			if (System.currentTimeMillis() - time >= mills) {
				System.out.println("任务已超时，立即结束");
				executeThread.interrupt();
				break;
			}

			try {
				executeThread.sleep(1);
			} catch (InterruptedException e) {
				System.out.println("任务执行被打断！");
				break;
			}
		}

		finished = false;
	}

	public static void main(String[] args) {
		ThreadService threadService = new ThreadService();
		long start = System.currentTimeMillis();
		threadService.execute(()->{
//			while (true) {
//
//			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		});

		threadService.shutdown(10000);
		long end = System.currentTimeMillis();
		System.out.println("end:" + end);
		System.out.println("start:" + start);
		System.out.println(end - start);
	}
}
