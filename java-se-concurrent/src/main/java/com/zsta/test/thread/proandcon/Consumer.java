package com.zsta.test.thread.proandcon;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class Consumer implements Runnable {

	private Resource resource;

	public Consumer(Resource resource) {
		this.resource = resource;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(500);
				System.out.println(Thread.currentThread().getName() + "--consumer--" + resource.remove());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
