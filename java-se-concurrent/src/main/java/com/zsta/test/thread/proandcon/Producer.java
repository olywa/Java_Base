package com.zsta.test.thread.proandcon;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class Producer implements Runnable{

	private Resource resource;
	public Producer(Resource resource) {
		this.resource = resource;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(500);

				resource.add();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
}
