package com.zsta.test.thread.proandcon;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class Resource {

	private int num;

	private int size=20;

//	private BlockingQueue<Integer> pool = new LinkedBlockingQueue<>(size);

	private LinkedList<Integer> pool = new LinkedList<>();

	private Random random = new Random();

	public synchronized void add() throws InterruptedException {
		if (num < size) {
			num++;
			int i = random.nextInt(100);
//			pool.add(i);
			pool.addFirst(i);
//			System.out.println(Thread.currentThread().getName() + "--produce--resource:" +i);

			notifyAll();
		} else {

			System.out.println(Thread.currentThread().getName() + "---pool is full, wait!");
			wait();
		}
	}

	public synchronized int remove() throws InterruptedException {
		if (num > 0) {
			num--;

//			int i= pool.take();
			int i= pool.removeLast();
			notifyAll();
			return i;
		} else {
			wait();
		}

		return -1;
	}
}
