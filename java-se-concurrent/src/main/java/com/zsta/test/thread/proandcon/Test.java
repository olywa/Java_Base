package com.zsta.test.thread.proandcon;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
public class Test {

	public static void main(String[] args) {
		Resource resource = new Resource();

		Producer p1 = new Producer(resource);
		Producer p2 = new Producer(resource);
		Producer p3 = new Producer(resource);

		Consumer c1 = new Consumer(resource);
		Consumer c2 = new Consumer(resource);

		ExecutorService service = Executors.newCachedThreadPool();
		service.execute(p1);
		service.execute(p2);
		service.execute(p3);

		service.execute(c1);
		service.execute(c2);
	}


}
