package com.zsta.test.thread.producerAndConsumer;

import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public class Consumer implements Runnable{

	private BlockingQueue<Integer> queue;

	public Consumer(BlockingQueue<Integer> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		while (true) {
			try {
				int i = queue.take();
				System.out.println(Thread.currentThread().getName() +"---consumer---" + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
