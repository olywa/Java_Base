package com.zsta.test.thread.producerAndConsumer;

import java.util.Random;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 17:31
 * To change this template use File | Settings | File Templates.
 */
public class Producer implements Runnable{

	private BlockingQueue<Integer> queue;

	public Producer(BlockingQueue<Integer> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		Random random = new Random();

		while (true) {
			int i = random.nextInt(100);

			queue.add(i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
