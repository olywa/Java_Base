package com.zsta.test.thread.producerAndConsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-03-05
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */
public class ProducerAndConsumerTeat {
	public static void main(String[] args) {

		BlockingQueue<Integer> queue = new LinkedBlockingDeque<>(20);

		Producer p1 = new Producer(queue);
		Producer p2 = new Producer(queue);
		Producer p3 = new Producer(queue);

		Consumer c1 = new Consumer(queue);
		Consumer c2 = new Consumer(queue);

		ExecutorService service = Executors.newCachedThreadPool();
		service.execute(p1);
		service.execute(p2);
		service.execute(p3);

		service.execute(c1);
		service.execute(c2);
	}
}
