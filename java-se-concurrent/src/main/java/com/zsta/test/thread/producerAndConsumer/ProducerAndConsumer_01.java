package com.zsta.test.thread.producerAndConsumer;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-18
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 *
 * wait和notify理解
 * 生产一个消费一个（生产者：1 消费者：1）
 *
 */
public class ProducerAndConsumer_01 {

	private int i=0;

	private final Object LOCK = new Object();

	private volatile boolean isProduced = false;

	public void produce() {
		synchronized (LOCK) {
			if (isProduced) {
				try {
					LOCK.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				i++;
				System.out.println("Produce: " + i);
				LOCK.notify();
				isProduced = true;
			}
		}
	}

	public void consumer() {
		synchronized (LOCK) {
			if (isProduced) {
				System.out.println("Consumer: " + i);
				LOCK.notify();
				isProduced = false;
			} else {
				try {
					LOCK.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		final ProducerAndConsumer_01 pad = new ProducerAndConsumer_01();
		new Thread(() -> {
			while(true) {
				pad.produce();
			}
		}).start();

		new Thread(() -> {
			while (true)
				pad.consumer();
		}).start();
	}
}
