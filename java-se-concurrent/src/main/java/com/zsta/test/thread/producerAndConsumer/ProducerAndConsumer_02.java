package com.zsta.test.thread.producerAndConsumer;

import java.util.stream.Stream;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-18
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 *
 * wait和notifyAll理解
 * 生产多个消费多个（生产者：1 消费者：1）
 *
 */
public class ProducerAndConsumer_02 {

	private int i=0;

	private final Object LOCK = new Object();

	private volatile boolean isProduced = false;

	public void produce(String name) {
		synchronized (LOCK) {
			if (isProduced) {
				try {
					LOCK.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				i++;
				System.out.println(name + ": " + i);
				LOCK.notify();
				isProduced = true;
			}
		}
	}

	public void consumer(String name) {
		synchronized (LOCK) {
			if (isProduced) {
				System.out.println(name + ": " + i);
				LOCK.notify();
				isProduced = false;
			} else {
				try {
					LOCK.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void produce() {
		synchronized (LOCK) {
			while (isProduced) {  //这里要使用while而不是if，多线程需循环判断，会导致重复生产（有生产的并未被消费），
				System.out.println(Thread.currentThread().getName() + ":" + i);
				LOCK.notifyAll();
				isProduced = false;
			}

			i++;
			isProduced = true;
			try {
				LOCK.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	public void consumer() {
		synchronized (LOCK) {
			while (isProduced) {
				System.out.println(Thread.currentThread().getName() + ":" + i);
				//若使用notify会导致多个线程都处于wait状态，类似假死: 并不能一定保证notify的produce，也可能是consumer,
				// 就会导致consumer notify的是consumer, consumer在获取lock后判断isProduce为false，则继续等待 导致假死
				LOCK.notifyAll();
				isProduced = true;
			}

			try {
				LOCK.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		final ProducerAndConsumer_02 pad = new ProducerAndConsumer_02();

		Stream.of("Producer_01", "Producer_02", "Producer_03").forEach((name) -> {
			new Thread(() -> {
				while (true) {
//					pad.produce(Thread.currentThread().getName());
					pad.produce();
				}
			}, name).start();
		});

		Stream.of("Consumer_01", "Consumer_02", "Consumer_03").forEach((name) -> {
			new Thread(() -> {
				while (true)
//					pad.consumer(Thread.currentThread().getName());
					pad.consumer();
			}, name).start();
		});
	}
}
