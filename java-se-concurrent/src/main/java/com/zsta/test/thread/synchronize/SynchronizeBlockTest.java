package com.zsta.test.thread.synchronize;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-16
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
public class SynchronizeBlockTest {

	public static void main(String[] args) {
		final TicketWindow ticketWindow = new TicketWindow();

		Thread t1 = new Thread(ticketWindow, "一号窗口");
		Thread t2 = new Thread(ticketWindow, "二号窗口");
		Thread t3 = new Thread(ticketWindow, "三号窗口");

		t1.start();
		t2.start();
		t3.start();
	}
}

class TicketWindow implements Runnable {

	private int ticketNo = 1;

	private static final int MAX = 500;

	private Object lock = new Object();

//	@Override
//	public synchronized void run() {
//		while (true) {
//			if (ticketNo > MAX) {
//				break;
//			}
//
//			try {
//				Thread.sleep(5);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//
//			System.out.println(Thread.currentThread().getName() + "当前出票号为：" + ticketNo++);
//		}
//	}

	@Override
	public void run() {
		while (true) {
//			在类方法上使用synchronize（同步块）和使用this做lock是效果一样的,
			synchronized (this) {
//			synchronized (lock) {
				if (ticketNo > MAX) {
					break;
				}

				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				System.out.println(Thread.currentThread().getName() + "当前出票号为：" + ticketNo++);
			}
		}
	}
}
