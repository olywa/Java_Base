package com.zsta.test.thread.synchronize;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-16
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
public class SynchronizeClassTest {

	static {
		synchronized (SynchronizeClassTest.class) {
			System.out.println(Thread.currentThread().getName() + "--class init...");
			try {
				Thread.sleep(10_000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		SynchronizeClassTest test = new SynchronizeClassTest();
		//由于static在初始化时，main线程抢占了锁（类对象），所以需要等待执行完成才能获取锁
		new Thread(() -> {
			test.m1();

		}, "T1").start();

		new Thread(() -> {
			test.m2();
		}, "T2").start();

		//调用方法并没有加锁的资源，所以直接执行
		new Thread(() -> {
			test.m3();
		}, "T3").start();
	}

	public synchronized static void m1() {
		System.out.println(Thread.currentThread().getName() + "--m1 invoked...");
		try {
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public synchronized static void m2() {
		System.out.println(Thread.currentThread().getName() + "--m2 invoked...");
		try {
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void m3() {
		System.out.println(Thread.currentThread().getName() + "--m3 invoked...");
		try {
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
