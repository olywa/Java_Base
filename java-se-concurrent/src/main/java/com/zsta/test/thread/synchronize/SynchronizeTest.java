package com.zsta.test.thread.synchronize;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-16
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public class SynchronizeTest {

	public static final Object lock = new Object();

	public static void main(String[] args) {
		Runnable runnable = () -> {
			synchronized (lock) {
				try {
					Thread.sleep(100_000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		Thread t1 = new Thread(runnable);
		Thread t2 = new Thread(runnable);
		Thread t3 = new Thread(runnable);

		t1.start();
		t2.start();
		t3.start();
	}
}
