package com.zsta.test.thread.threadpool;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-09-18
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
public class MyThreadPool_01 {

//	private static final LinkedList POOL = new LinkedList();
	private static final ConcurrentHashMap<String, Object> POOL = new ConcurrentHashMap<>();

	private static final int MAX = 5;

	public static void main(String[] args) {
		List<Thread> worker = new ArrayList<>();
		Arrays.asList("T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9", "T10").stream()
				.map(MyThreadPool_01::createCaptureThread)
				.forEach((t) -> {
					t.start();

					worker.add(t);
				});

		worker.forEach((t) -> {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});

		System.out.println("all worker completed!");
	}

	private static Thread createCaptureThread(String name) {
		return new Thread(() -> {

			Optional.of("The Worker [" + Thread.currentThread().getName() + "] begin capture data.").ifPresent(System.out::println);

			synchronized (POOL) {
				while (POOL.size() > MAX) {
					try {
						POOL.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
//				POOL.addLast(new Object());
				POOL.put(Thread.currentThread().getName(), new Object());
			}

			System.out.println(Thread.currentThread().getName() + " working....");
			try {
				Thread.sleep(5_000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}


			synchronized (POOL) {
				System.out.println(Thread.currentThread().getName() + " to end.");
				POOL.remove(Thread.currentThread().getName());
				POOL.notifyAll();
			}

		}, name);
	}
}
