package com.zsta.test.thread.threadpool;


import org.junit.Test;

import java.util.concurrent.*;


/**
 * Created with IntelliJ IDEA.
 * User: zhangshaotao
 * Date: 2021-01-06
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */

public class ThreadPoolTest {

//	new ThreadPoolExecutor()

//	参数列表
//	1. coolPoolSize: 线程池中维护的线程的最少数量
//	2. maxinumPoolSize: 线程池中维护的线程的最大数量
//	3. keepAliveTime: 线程池中线程空闲的时间数
//	4. unit: 空闲时间单位
//	5. workQueue: 任务等待队列
//	LinkedBlockingDeque无界队列（也可指定长度，默认是MAX_INT） ArrayBlockingQueue有界队列 SynchronousQueue无缓冲区队列
//	6. handler: 任务处理策略有以下4种
	//ThreadPoolExecutor.AbortPolicy()（系统默认）：抛出java.util.concurrent.RejectedExecutionException异常
	//ThreadPoolExecutor.CallerRunsPolicy()：若线程没有关闭，则正常处理任务
	//ThreadPoolExecutor.DiscardOldestPolicy()：抛弃旧的任务
	//ThreadPoolExecutor.DiscardPolicy()：抛弃当前的任务

//	参数使用说明：
//	1. 如果此时线程池中的数量小于corePoolSize，即使线程池中的线程都处于空闲状态，也要创建新的线程来处理被添加的任务。
//	2. 如果此时线程池中的数量等于 corePoolSize，但是缓冲队列 workQueue未满，那么任务被放入缓冲队列。
//	3. 如果此时线程池中的数量大于corePoolSize，缓冲队列workQueue满，并且线程池中的数量小于maximumPoolSize，建新的线程来处理被添加的任务。
//	4. 如果此时线程池中的数量大于corePoolSize，缓冲队列workQueue满，并且线程池中的数量等于maximumPoolSize，那么通过 handler所指定的策略来处理此任务。
//	   也就是：处理任务的优先级为：核心线程corePoolSize、任务队列workQueue、最大线程maximumPoolSize，如果三者都满了，使用handler处理被拒绝的任务。
//	5. 当线程池中的线程数量大于 corePoolSize时，如果某线程空闲时间超过keepAliveTime，线程将被终止。

	/***
	 * @Description:
	 *
	 * 若池中数量小于coresize, 即使有线程空闲也会新建线程执行任务
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午7:16
	 */
	@Test
	public void test1() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				10,
				3,
				TimeUnit.SECONDS,
				new LinkedBlockingDeque<>(),
				new ThreadPoolExecutor.AbortPolicy());

		this.poolExecute(executor, new Thread(new Task2Sec()));
	}

	/***
	 * @Description:
	 *
	 *  若大于coresize, 且queue未满则放入queue中；此时池中线程数量还是coresize
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午7:26
	 */
	@Test
	public void test2() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				10,
				3,
				TimeUnit.SECONDS,
				new LinkedBlockingDeque<>(),  // 若缓冲队列满了，但是池中线程数量小于maxsize，则新增线程处理任务
				new ThreadPoolExecutor.AbortPolicy());

		this.poolExecute(executor, new Thread(new Task10Sec()));

	}

	/***
	 * @Description:
	 *
	 *  池中线程数量大于coresize小于maxsize，, 且缓冲队列满了，则新增线程处理任务
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午7:26
	 */
	@Test
	public void test3() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				10,
				3,
				TimeUnit.SECONDS,
//				new LinkedBlockingQueue<>(1),
				new ArrayBlockingQueue<>(1),
				new ThreadPoolExecutor.AbortPolicy());

		this.poolExecute(executor, new Thread(new Task10Sec()));

	}

	/***
	 * @Description:
	 *
	 *  若缓冲队列满了，且池中线程数量大于maxsize，则执行handler
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午7:26
	 */
	@Test
	public void test4() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				2,
				3,
				3,
				TimeUnit.SECONDS,
				new LinkedBlockingDeque<>(1),
				new ThreadPoolExecutor.AbortPolicy());

		this.poolExecute(executor, new Thread(new Task10Sec()));

	}

	/***
	 * @Description:
	 *
	 *  SychronousQueue是个无缓冲队列，可设置公平性(公平：FIFO，非公平：随机)，
	 *  新加入的任务会按照core与max大小关系，决定是否执行任务或是执行handler
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午7:26
	 */
	@Test
	public void test5() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				8,
				3,
				TimeUnit.SECONDS,
				new SynchronousQueue<>(),
				new ThreadPoolExecutor.AbortPolicy());

		this.poolExecute(executor, new Thread(new Task2Sec()));

		System.out.println("-------------------------------");
		this.poolExecute(executor, new Thread(new Task10Sec()));

	}

	/***
	 * @Description:
	 *
	 * 若线程池未关闭则任务正常执行
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午8:12
	 */
	@Test
	public void test6() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				8,
				3,
				TimeUnit.SECONDS,
				new SynchronousQueue<>(),
				new ThreadPoolExecutor.CallerRunsPolicy());

		this.poolExecute(executor, new Thread(new Task10Sec()));

	}

	/***
	 * @Description:
	 *
	 * 放弃队列中未执行的任务，放弃thread-8（此时只能只用有界队列才能达到测试目的）
	 * 注意使用SynchronousQueue会有StackOverFlow的异常
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午8:12
	 */
	@Test
	public void test7() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				7,
				3,
				TimeUnit.SECONDS,
				new LinkedBlockingDeque<>(1),
				new ThreadPoolExecutor.DiscardOldestPolicy());

		this.poolExecute(executor, new Thread(new Task5Sec()));

	}
	/***
	 * @Description:
	 *
	 * 放弃要加入的任务（thread-9）
	 *
	 * @param
	 * @return: void
	 * @Author: zhangshaotao
	 * @Date: 2021/1/6 下午8:12
	 */
	@Test
	public void test8() throws InterruptedException {

		ThreadPoolExecutor executor = new ThreadPoolExecutor(
				6,
				8,
				3,
				TimeUnit.SECONDS,
				new SynchronousQueue<>(),
				new ThreadPoolExecutor.DiscardPolicy());

		this.poolExecute(executor, new Thread(new Task5Sec()));

	}

	private void poolExecute(ThreadPoolExecutor executor, Thread myRunnable) throws InterruptedException {

		System.out.println("---先开三个---");
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		System.out.println("核心线程数" + executor.getCorePoolSize());
		System.out.println("线程池数" + executor.getPoolSize());
		System.out.println("队列任务数" + executor.getQueue().size());
		System.out.println("---再开三个---");
		Thread.sleep(3000);
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		System.out.println("核心线程数" + executor.getCorePoolSize());
		System.out.println("线程池数" + executor.getPoolSize());
		System.out.println("队列任务数" + executor.getQueue().size());
		System.out.println("---又开三个---");
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		executor.execute(myRunnable);
		System.out.println("核心线程数" + executor.getCorePoolSize());
		System.out.println("线程池数" + executor.getPoolSize());
		System.out.println("队列任务数" + executor.getQueue().size());
		Thread.sleep(8000);
		System.out.println("----8秒之后----");
		System.out.println("核心线程数" + executor.getCorePoolSize());
		System.out.println("线程池数" + executor.getPoolSize());
		System.out.println("队列任务数" + executor.getQueue().size());
	}
}

class Task5Sec implements Runnable {

	@Override
	public void run() {
		try {
			Thread.sleep(5000);
			System.out.println(Thread.currentThread().getName() + " completed....");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class Task10Sec implements Runnable {

	@Override
	public void run() {
		try {
			Thread.sleep(10000);
			System.out.println(Thread.currentThread().getName() + " completed....");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

class Task2Sec implements Runnable {

	@Override
	public void run() {
		try {
			Thread.sleep(2000);
			System.out.println(Thread.currentThread().getName() + " completed....");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

