package com.zsta.test;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-11-13
 * Time: 21:22
 * To change this template use File | Settings | File Templates.
 */
public class Test {

	public static void main(String[] args) {
		int[] nums1 = {1};
		int[] nums2 = {};
		if (nums1.length == 0 || nums2.length == 0) {
//			return 0d;
		}

		int[] nums = new int[nums1.length + nums2.length];
		int index1 = 0;
		int index2 = 0;
		for (int i=0; i<nums.length; i++) {
			if( index1 < nums1.length && nums1[index1] <= nums2[index2]) {
				nums[i] = nums1[index1];
				index1++;
			} else {
				nums[i] = nums2[index2];
				if (index2 + 1 < nums2.length) {
					index2++;
				}
			}
		}

		double mid = 0d;
		if (nums.length % 2 == 0 ) {
			int numMid = nums.length / 2;
			mid = (double)(nums[numMid-1] + nums[numMid]) / 2.0d;
		} else {
			mid = new Double(nums[(nums.length + 1)/4]);
		}
		System.out.println(mid);
	}
}
