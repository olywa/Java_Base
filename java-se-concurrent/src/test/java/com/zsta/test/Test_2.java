package com.zsta.test;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2019-11-22
 * Time: 11:14
 * To change this template use File | Settings | File Templates.
 */
public class Test_2 {
	public static void main(String[] args) {
		int a = 1;
		System.out.println(0 ^ a);
		System.out.println(0 & a);
		System.out.println(0 | a);
		System.out.println(~ a);
//		3---正数的原反补是一致的
//		原码
//		000000000000000000000000000X00011
//		反码
//		00000000000000000000000000000011
//		补码
//		00000000000000000000000000000011
		System.out.println(Integer.toBinaryString(3));
//		-3
//		原码
//		10000000000000000000000000000011
//		反码
//		11111111111111111111111111111100
//		补码 -- Java中的负数使用补码表示
//		11111111111111111111111111111101
		System.out.println(Integer.toBinaryString(-3));
//		-3 / 2*2 == -1
		System.out.println((-3 >> 2) + "的反码为：" + Integer.toBinaryString(-3 >> 2));
		System.out.println(Integer.toBinaryString(-3 << 2));

		System.out.println((-3 >>> 2) + "无符号右移：" + Integer.toBinaryString(-3 >>> 2));
//		没有无符号左移 左移是不会涉及到最高位（符号位）
//		System.out.println((-3 <<< 2) + "无符号右移：" + Integer.toBinaryString(-100000000 <<< 2));

//		如果操作类型低于int类型，比如byte，char等，先将其转化为int类型在进行移位。
//		对于int类型的移位，如果移动位数超过32位，则让位数对32取余，然后进行运行，即a>>33 == a>>1 a>>32 ==a
//		同样如果对于long类型的移位，移动位数超过64，则也需要对移动位数进行处理。
		System.out.println(4 << 36);
		System.out.println((long)1214 >> 66);
		System.out.println((long)1214 >> 2);
	}
}
