package com.test.zsta;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 19:54
 * To change this template use File | Settings | File Templates.
 */
public class Utils {

	public static Object toObject(byte[] bytes){
		Object object = null;
		try {
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);// 创建ByteArrayInputStream对象
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);// 创建ObjectInputStream对象
			object = objectInputStream.readObject();// 从objectInputStream流中读取一个对象
			byteArrayInputStream.close();// 关闭输入流
			objectInputStream.close();// 关闭输入流
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return object;// 返回对象
	}

	public static byte[] toByteArray(Object object) {
		byte[] bytes = null;
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(object);
			objectOutputStream.flush();
			bytes = byteArrayOutputStream.toByteArray();
			byteArrayOutputStream.close();
			objectOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}
}
