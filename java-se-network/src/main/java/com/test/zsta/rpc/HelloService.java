package com.test.zsta.rpc;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public interface HelloService {

	String sayHi(String name);
}
