package com.test.zsta.rpc;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */
public class HelloServiceImpl implements HelloService {
	@Override
	public String sayHi(String name) {
		return "Hi, " + name;
	}
}
