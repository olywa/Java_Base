package com.test.zsta.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public class MyRPCServer implements RPCServer {

//	private static ExecutorService executor = new ThreadPoolExecutor();
	private static ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	//模拟注册中心
	private static  final Map<String, Class> serviceRegistry = new HashMap<>();

	private static boolean isRunning = false;

	private static int port;

	public MyRPCServer(int port) {
		this.port = port;
	}

	@Override
	public void stop() {
		isRunning = false;
		executor.shutdown();
	}

	@Override
	public void start() throws IOException {
		ServerSocket server = new ServerSocket();
		server.bind(new InetSocketAddress(port));
		System.out.println("server start");

		try {
			while (true) {
				//监听tcp连接（此处阻塞）
				Socket requestSocket = server.accept();
				System.out.println("receive ask...");
				//接到tcp请求后将其封装为task，并在线程池中执行任务
				executor.execute(new ServiceTask(requestSocket));

			}
		} finally {
			server.close();
		}

	}

	@Override
	public void register(Class serviceInterface, Class impl) {
		serviceRegistry.put(serviceInterface.getName(), impl);
	}

	@Override
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public int getPort() {
		return 0;
	}

	private class ServiceTask implements Runnable {

		Socket client = null;

		ServiceTask(Socket client) {
			this.client = client;
		}

		@Override
		public void run() {
			ObjectInputStream inputStream = null;
			ObjectOutputStream outputStream = null;

			try {
				//解析客户端发送的请求（二进制流）
				inputStream = new ObjectInputStream(client.getInputStream());
				//请求的接口的类名
				String serviceName = inputStream.readUTF();
				//请求接口的名称
				String methodName = inputStream.readUTF();
				//请求传递参数类型
				Class<?>[] parameterTypes = (Class<?>[])inputStream.readObject();
				//参数
				Object[] arguments = (Object[]) inputStream.readObject();
				//从注册中心获取服务接口
				Class serviceClass = serviceRegistry.get(serviceName);
				if (serviceClass == null) {
					throw new ClassNotFoundException(serviceName + "not found");
				}

				Method method = serviceClass.getMethod(methodName, parameterTypes);
				//利用反射调用接口
				Object result = method.invoke(serviceClass.newInstance(), arguments);

				//将结果编码返回客户端
				outputStream = new ObjectOutputStream(client.getOutputStream());
				outputStream.writeObject(result);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					outputStream.flush();
					outputStream.close();
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		RPCServer rpcServer = new MyRPCServer(1080);
		rpcServer.register(HelloService.class, HelloServiceImpl.class);
		try {
			rpcServer.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
