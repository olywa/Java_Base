package com.test.zsta.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 11:33
 * To change this template use File | Settings | File Templates.
 */
public class RPCClient<T> {
	public static <T> T getRemoteProxyObj(final Class<?> serviceInterface, final InetSocketAddress address) {

		return (T) Proxy.newProxyInstance(serviceInterface.getClassLoader(), new Class<?>[]{serviceInterface}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) {

				Socket socket = null;
				ObjectOutputStream outputStream = null;
				ObjectInputStream inputStream = null;

				Object result = null;
				try {
					socket = new Socket();
					socket.connect(address);

					outputStream = new ObjectOutputStream(socket.getOutputStream());
					outputStream.writeUTF(serviceInterface.getName());
					outputStream.writeUTF(method.getName());
					outputStream.writeObject(method.getParameterTypes());
					outputStream.writeObject(args);

					inputStream = new ObjectInputStream(socket.getInputStream());

					result = inputStream.readObject();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						outputStream.close();
						inputStream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				return result;
			}
		});
	}
}
