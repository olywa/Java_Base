package com.test.zsta.rpc;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */
public interface RPCServer {

	void stop();

	void start() throws IOException;

	void register(Class serviceInterface, Class impl);

	boolean isRunning();

	int getPort();
}
