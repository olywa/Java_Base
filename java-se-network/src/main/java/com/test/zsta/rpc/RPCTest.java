package com.test.zsta.rpc;

import com.test.zsta.rpc.nio.RPCClient_NIO;

import java.net.InetSocketAddress;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */
public class RPCTest {
	public static void main(String[] args) {
		HelloService service = RPCClient.getRemoteProxyObj(HelloService.class, new InetSocketAddress("localhost",1080));
		System.out.println(service.sayHi("Jame"));  //第一次请求

		System.out.println(service.sayHi("Jack"));  //第二次请求
//		HelloService service = RPCClient_NIO.getRemoteProxyObj(HelloService.class, new InetSocketAddress("localhost",1080));
//		System.out.println(service.sayHi("Jame"));  //第一次请求
//
//		System.out.println(service.sayHi("Jack"));  //第二次请求
	}
}
