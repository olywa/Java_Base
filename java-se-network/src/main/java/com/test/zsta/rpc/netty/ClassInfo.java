package com.test.zsta.rpc.netty;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */
public class ClassInfo implements Serializable{

	private static final long serialVersionUID = -2683279326545688832L;

	private String className;
	private String methodName;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Class<?>[] getTypes() {
		return types;
	}

	public void setTypes(Class<?>[] types) {
		this.types = types;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	private Class<?>[] types;
	private Object[] params;
}
