package com.test.zsta.rpc.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 15:39
 * To change this template use File | Settings | File Templates.
 */
public class InvokerHandler extends ChannelInboundHandlerAdapter {

	public void channelRead(ChannelHandlerContext channelHandlerContext, Object msg) {
		System.out.println("server: receive msg!");
		ClassInfo classInfo = (ClassInfo) msg;
		Class serviceClass = null;
		if (NettyRPCServer.registerServices.containsKey(classInfo.getClassName())) {
			serviceClass = NettyRPCServer.registerServices.get(classInfo.getClassName());
			Object invokeResult = null;
			try {

				Method method = serviceClass.getMethod(classInfo.getMethodName(), classInfo.getTypes());
				invokeResult = method.invoke(serviceClass.newInstance(), classInfo.getParams());
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}

			channelHandlerContext.write(invokeResult);
			channelHandlerContext.flush();
			channelHandlerContext.close();
		} else {
			try {
				throw new ClassNotFoundException("service not found");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

	}

}
