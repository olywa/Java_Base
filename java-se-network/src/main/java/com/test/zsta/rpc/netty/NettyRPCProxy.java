package com.test.zsta.rpc.netty;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 16:19
 * To change this template use File | Settings | File Templates.
 */
public class NettyRPCProxy {

	public static <T> T call(final Class<?> serviceInterface, final InetSocketAddress address) {
		return (T) Proxy.newProxyInstance(serviceInterface.getClassLoader(), new Class<?>[]{serviceInterface}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

				EventLoopGroup eventLoopGroup = null;
				ResultHandler resultHandler = new ResultHandler();
				try {
					ClassInfo classInfo = new ClassInfo();
					classInfo.setClassName(serviceInterface.getName());
					classInfo.setMethodName(method.getName());
					classInfo.setParams(args);
					classInfo.setTypes(method.getParameterTypes());

					eventLoopGroup = new NioEventLoopGroup();

					Bootstrap bootstrap = new Bootstrap();
					bootstrap.group(eventLoopGroup)
							.channel(NioSocketChannel.class)
							.option(ChannelOption.TCP_NODELAY, true)
							.handler(new ChannelInitializer<SocketChannel>() {

								@Override
								protected void initChannel(SocketChannel channel) throws Exception {
									ChannelPipeline pipeline = channel.pipeline();
									pipeline.addLast("frameDecoder", new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
									pipeline.addLast("frameEncoder", new LengthFieldPrepender(4));
									pipeline.addLast("encoder", new ObjectEncoder());
									pipeline.addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
									pipeline.addLast("handler",resultHandler);

								}
							});

					ChannelFuture channelFuture = bootstrap.connect(address).sync();
					channelFuture.channel().writeAndFlush(classInfo).sync();
					channelFuture.channel().closeFuture().sync();
				} finally {
					eventLoopGroup.shutdownGracefully();
				}
				return resultHandler.getResponse();
			}
		});
	}
}

class ResultHandler extends ChannelInboundHandlerAdapter{

	public Object response;

	public Object getResponse() {
		return response;
	}

	public void channelRead(ChannelHandlerContext channelHandlerContext, Object msg) {
		response = msg;
		System.out.println("接收到服务端的消息:" + msg);
	}
}
