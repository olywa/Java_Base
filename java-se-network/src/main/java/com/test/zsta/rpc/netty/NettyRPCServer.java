package com.test.zsta.rpc.netty;

import com.test.zsta.rpc.HelloService;
import com.test.zsta.rpc.HelloServiceImpl;
import com.test.zsta.rpc.RPCServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 14:27
 * To change this template use File | Settings | File Templates.
 */
public class NettyRPCServer implements RPCServer {

	public static ConcurrentHashMap<String, Class> registerServices = new ConcurrentHashMap();

	private int port=2080;

	@Override
	public void stop() {

	}

	@Override
	public void start() throws IOException {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();

		ServerBootstrap serverBootstrap = new ServerBootstrap();
		serverBootstrap.localAddress(port)
				.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(new ChannelHandler())
				.option(ChannelOption.SO_BACKLOG, 1024)
				.childOption(ChannelOption.SO_KEEPALIVE, true);

		try {
			ChannelFuture future = serverBootstrap.bind().sync();
			future.channel().closeFuture().sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void register(Class serviceInterface, Class impl) {

	}

	@Override
	public boolean isRunning() {
		return false;
	}

	@Override
	public int getPort() {
		return 0;
	}

	private class ChannelHandler extends ChannelInitializer<SocketChannel> {

		@Override
		public void initChannel(SocketChannel channel) throws Exception {
			System.out.println("server init...");
			ChannelPipeline pipeline = channel.pipeline();
			pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
			pipeline.addLast(new LengthFieldPrepender(4));
			pipeline.addLast("encoder", new ObjectEncoder());
			pipeline.addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
			pipeline.addLast(new InvokerHandler());
		}
	}

	public static void main(String[] args) {
		registerServices.put(HelloService.class.getName(), HelloServiceImpl.class);

		NettyRPCServer server = new NettyRPCServer();
		try {
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
