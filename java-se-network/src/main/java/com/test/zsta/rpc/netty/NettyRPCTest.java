package com.test.zsta.rpc.netty;

import com.test.zsta.rpc.HelloService;

import java.net.InetSocketAddress;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 17:19
 * To change this template use File | Settings | File Templates.
 */
public class NettyRPCTest {

	public static void main(String[] args) {
		HelloService helloService = NettyRPCProxy.call(HelloService.class, new InetSocketAddress(2080));
		System.out.println(helloService.sayHi("zsta"));
		System.out.println(helloService.sayHi("Jame"));
	}
}
