package com.test.zsta.rpc.nio;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 18:46
 * To change this template use File | Settings | File Templates.
 */
public class Batch implements Serializable {

	private static final long serialVersionUID = 4742740134352172949L;
	//请求的接口的类名
	private String serviceName;
	//请求接口的名称
	private String methodName;
	//请求传递参数类型
	public  String     getServiceName() {
		return serviceName;
	}public void       setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}public String     getMethodName() {
		return methodName;
	}public void       setMethodName(String methodName) {
		this.methodName = methodName;
	}public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}public void       setParameterTypes(Class<?>[] parameterTypes) {
		this.parameterTypes = parameterTypes;
	}public Object[]   getArguments() {
		return arguments;
	}public void       setArguments(Object[] arguments) {
		this.arguments = arguments;
	}private Class<?> [] parameterTypes;
	//参数
	private Object[] arguments;
}
