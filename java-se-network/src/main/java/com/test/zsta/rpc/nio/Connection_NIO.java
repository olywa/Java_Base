package com.test.zsta.rpc.nio;

import com.test.zsta.Utils;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 16:04
 * To change this template use File | Settings | File Templates.
 */
public class Connection_NIO {

	private String hostName;

	private int port;

	public Connection_NIO() {

	}

	public Connection_NIO(String hostName, int port) {
		this.hostName = hostName;
		this.port = port;
	}

	public Selector init() throws IOException {
		ServerSocketChannel serverSocketChannel = null;
		Selector selector = null;
		serverSocketChannel = ServerSocketChannel.open();
		selector = Selector.open();
		serverSocketChannel.bind(new InetSocketAddress(hostName, port));
		serverSocketChannel.configureBlocking(false);
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		System.out.println("服务端已初始化...");

		return selector;
	}

//	public void listen() throws IOException {
//		selector.select();
//		Set<SelectionKey> selectionKeys = selector.selectedKeys();
//		while (selectionKeys.iterator().hasNext()) {
//			SelectionKey selectionKey = selectionKeys.iterator().next();
//			if (selectionKey.isAcceptable()) {
//				connect(selectionKey);
//			} else if (selectionKey.isReadable()) {
//				read(selectionKey);
//			} else if (selectionKey.isWritable()) {
//				write(selectionKey);
//			} else if (selectionKey.isConnectable()) {
//				close(selectionKey);
//			}
//
//			selectionKeys.iterator().remove();
//		}
//	}

	public void connect(SelectionKey selectionKey, Selector selector) throws IOException {
		ServerSocketChannel serverSocketChannel = (ServerSocketChannel) selectionKey.channel();
		serverSocketChannel.accept()
					.configureBlocking(false)
					.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
		System.out.println("连接建立...");
	}

	public Object read(SelectionKey selectionKey) throws IOException {
		SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
		ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
		int readIndex = socketChannel.read(byteBuffer);
		List byteList = new ArrayList<>(readIndex);
		while (readIndex != -1) {
			byteBuffer.flip();
			byteList.add(byteBuffer.get());
			readIndex = socketChannel.read(byteBuffer);
		}

		byte[] objectBytes = new byte[byteList.size()];
		return Utils.toObject(objectBytes);
	}

	public void write(SelectionKey selectionKey, Object object) throws IOException {
		SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
		byte[] bytes = Utils.toByteArray(object);
		ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
		int writeIndex = 0;
		while (writeIndex <= bytes.length) {
			byteBuffer.flip();
			byteBuffer.put(Arrays.copyOfRange(bytes, writeIndex, byteBuffer.capacity()));
			socketChannel.write(byteBuffer);
			writeIndex += byteBuffer.capacity();
		}
	}

	public void close(SelectionKey selectionKey) {
		selectionKey.attachment();
	}

}
