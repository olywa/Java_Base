package com.test.zsta.rpc.nio;

import com.test.zsta.Utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 19:45
 * To change this template use File | Settings | File Templates.
 */
public class RPCClient_NIO {

	public static <T> T getRemoteProxyObj(final Class<?> serviceInterface, final InetSocketAddress address) {


		return (T) Proxy.newProxyInstance(serviceInterface.getClassLoader(), new Class<?>[]{serviceInterface}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				SocketChannel socketChannel = SocketChannel.open(address);
				Selector selector = Selector.open();
				socketChannel.configureBlocking(false);
				socketChannel.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
				ByteBuffer byteBuffer = ByteBuffer.allocate(1024);

				Batch batch = new Batch();
				batch.setServiceName(serviceInterface.getName());
				batch.setMethodName(method.getName());
				batch.setArguments(args);
				batch.setParameterTypes(method.getParameterTypes());

				byte[] bytes = Utils.toByteArray(batch);
				int writeIndex = 0;
				while (writeIndex <= bytes.length) {
					byteBuffer.flip();
					byteBuffer.clear();
					byteBuffer.put(Arrays.copyOfRange(bytes, writeIndex, byteBuffer.capacity()));
					socketChannel.write(byteBuffer);
					writeIndex += byteBuffer.capacity();
				}

//				byteBuffer.flip();
//				byteBuffer.put(Utils.toByteArray(batch));
//				socketChannel.write(byteBuffer);
//				byteBuffer.clear();

				selector.select();
				Set<SelectionKey> selectionKeys = selector.selectedKeys();
				Object result = null;
				while (selectionKeys.iterator().hasNext()) {
					SelectionKey selectionKey = selectionKeys.iterator().next();
					if (selectionKey.isReadable()) {
						SocketChannel channel = (SocketChannel) selectionKey.channel();
						ByteBuffer readBuffer = ByteBuffer.allocate(1024);
						int readIndex = channel.read(readBuffer);
						List byteList = new ArrayList<>(readIndex);
						while (readIndex != -1) {
							readBuffer.flip();
							byteList.add(readBuffer.get());
							readIndex = channel.read(readBuffer);
						}

						byte[] objectBytes = new byte[byteList.size()];
						result = Utils.toObject(objectBytes);
					} else if (selectionKey.isConnectable()) {
						selectionKey.attachment();
					}

				}

				return result;
			}
		});
	}
}
