package com.test.zsta.rpc.nio;

import com.test.zsta.rpc.HelloService;
import com.test.zsta.rpc.HelloServiceImpl;
import com.test.zsta.rpc.RPCServer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-21
 * Time: 15:44
 * To change this template use File | Settings | File Templates.
 */
public class RPCServer_NIO implements RPCServer{

	private static final ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	private static final Map<String, Class> serviceRegistry = new HashMap<>();

	private static boolean isRunning = false;

	private int port;

	public RPCServer_NIO(int port) {
		this.port = port;
	}

	@Override
	public void stop() {
		isRunning = false;
	}

	@Override
	public void start() throws IOException {

		Connection_NIO connection = new Connection_NIO("localhost", port);
		Selector selector = connection.init();

		while (selector.select() > 0) {

			Set<SelectionKey> selectionKeys = selector.selectedKeys();
			Batch batch = null;
			Iterator<SelectionKey> iterator = selectionKeys.iterator();
			while (iterator.hasNext()) {
				SelectionKey selectionKey = iterator.next();
				if (selectionKey.isAcceptable()) {
					connection.connect(selectionKey, selector);
				} else if (selectionKey.isReadable()) {
					batch = (Batch) connection.read(selectionKey);
				} else if (selectionKey.isWritable()) {
					connection.write(selectionKey, callProc(batch));
				} else if (selectionKey.isConnectable()) {
					connection.close(selectionKey);
				}

				iterator.remove();
			}
		}

	}

	@Override
	public void register(Class serviceInterface, Class impl) {
		serviceRegistry.put(serviceInterface.getName(), impl);
	}

	@Override
	public boolean isRunning() {
		return isRunning;
	}

	@Override
	public int getPort() {
		return port;
	}

	private Object callProc(Batch batch) {

		Object result = null;
		try {
			//从注册中心获取服务接口
			Class serviceClass = serviceRegistry.get(batch.getServiceName());
			if (serviceClass == null) {
				throw new ClassNotFoundException(batch.getServiceName() + "not found");
			}

			Method method = serviceClass.getMethod(batch.getMethodName(), batch.getParameterTypes());
			//利用反射调用接口
			result = method.invoke(serviceClass.newInstance(), batch.getArguments());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} finally {


		}

		return result;
	}

	public static void main(String[] args) {
		RPCServer_NIO rpcServer = new RPCServer_NIO(1080);
		rpcServer.register(HelloService.class, HelloServiceImpl.class);
		try {
			rpcServer.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
