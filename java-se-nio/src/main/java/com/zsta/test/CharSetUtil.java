package com.zsta.test;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-22
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
public class CharSetUtil {

	public static String decode(ByteBuffer buffer, String charsetName) {
		Charset charset = Charset.forName(charsetName);
		CharBuffer msg = charset.decode(buffer);
		return msg.toString();
	}

	public static ByteBuffer encode(String msg, String charsetName) {
		Charset charset = Charset.forName(charsetName);
		ByteBuffer byteBuffer = charset.encode(msg);
		return byteBuffer;
	}

}
