package com.zsta.test.netty;

import io.netty.buffer.ChannelBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-23
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class ServerHandler extends ChannelInboundHandlerAdapter {
	@Override
	public void inboundBufferUpdated(ChannelHandlerContext channelHandlerContext) throws Exception {

	}

	@Override
	public ChannelBuf newInboundBuffer(ChannelHandlerContext channelHandlerContext) throws Exception {
		return null;
	}

	@Override
	public void freeInboundBuffer(ChannelHandlerContext channelHandlerContext, ChannelBuf channelBuf) throws Exception {

	}
}
