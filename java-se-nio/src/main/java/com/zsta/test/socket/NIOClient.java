package com.zsta.test.socket;

import com.zsta.test.CharSetUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-22
 * Time: 11:44
 * To change this template use File | Settings | File Templates.
 */
public class NIOClient {

	public static void main(String[] args) throws IOException {
		InetSocketAddress address = new InetSocketAddress("localhost",2080);
		SocketChannel socketChannel = SocketChannel.open();
		Selector selector = Selector.open();

		socketChannel.configureBlocking(false)
				.register(selector, SelectionKey.OP_CONNECT);

		socketChannel.connect(address);

//		new Thread(() -> {

			try {
				while (selector.select() > 0) {
					Set<SelectionKey> selectionKeySet = selector.selectedKeys();
					Iterator<SelectionKey> iterator = selectionKeySet.iterator();
					while (iterator.hasNext()) {
						SelectionKey selectionKey = iterator.next();

						if (selectionKey.isConnectable()) {
							System.out.println("client connect");
							SocketChannel client = (SocketChannel) selectionKey.channel();
							ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
							// 判断此通道上是否正在进行连接操作。
							// 完成套接字通道的连接过程。
							if (client.isConnectionPending()) {
								client.finishConnect();
								System.out.println("完成连接!");
								byteBuffer.clear();
								byteBuffer.put("Hello, Server".getBytes());
								byteBuffer.flip();
								client.write(byteBuffer);
							}

							client.register(selector, SelectionKey.OP_WRITE);
						}

						if (selectionKey.isReadable()) {
							SocketChannel channel = (SocketChannel) selectionKey.channel();
							ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
							List<Byte> resultList = new LinkedList<>();
							while (channel.read(byteBuffer) > 0) {
								byteBuffer.flip();
								resultList.add(byteBuffer.get());
							}

							byte[] bytes = new byte[resultList.size()];
							for (int i=0; i<resultList.size(); i++) {
								bytes[i] = resultList.get(i);
							}

							System.out.println(new String(bytes));
							channel.register(selector, SelectionKey.OP_WRITE);
						}

						if (selectionKey.isWritable()) {

							SocketChannel channel = (SocketChannel) selectionKey.channel();

							ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
							Scanner scanner = new Scanner(System.in);
							byteBuffer.flip();
							byteBuffer.clear();

							System.out.println("please input...");
							String input = "client: " + scanner.next();
//								byteBuffer.put(CharSetUtil.encode(input, "UTF-8"));
							byteBuffer.put(input.getBytes());
							channel.write(byteBuffer);
							System.out.println(input);

							channel.register(selector, SelectionKey.OP_READ);
						}

						iterator.remove();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
//		}).start();


	}
}
