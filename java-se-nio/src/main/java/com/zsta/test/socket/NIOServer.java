package com.zsta.test.socket;

import com.zsta.test.CharSetUtil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-22
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
public class NIOServer {

	private String hostName = "localhost";
	private int port = 2080;

	void init() throws IOException {
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();

		//获取serversocket 并绑定端口号
		ServerSocket serverSocket = serverSocketChannel.socket();
		InetSocketAddress address = new InetSocketAddress(hostName, port);
		serverSocket.bind(address);

		//获取selector
		Selector selector = Selector.open();

		serverSocketChannel.configureBlocking(false);
		//将serverchannel注册到selector(可读可写)
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

		while (selector.select() > 0) {
			Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
			while (iterator.hasNext()) {
				SelectionKey selectionKey = iterator.next();
				if (selectionKey.isAcceptable()) {

					ServerSocketChannel serverChannel = (ServerSocketChannel) selectionKey.channel();
					SocketChannel socketChannel = serverChannel.accept();
					socketChannel.configureBlocking(false);
					socketChannel.register(selector, SelectionKey.OP_READ);

					System.out.println("connection build...");
				}
				if (selectionKey.isReadable()) {
					SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
					ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
//					String content = "";
//					while (socketChannel.read(byteBuffer) > 0) {
//						byteBuffer.clear();
//						byteBuffer.flip();
//						while (byteBuffer.remaining() > 0) {
//
//							String ct = CharSetUtil.decode(byteBuffer, "UTF-8");
//							System.out.println(ct);
//							content += ct;
//						}
//					}

					byteBuffer.clear();
					byteBuffer.flip();
					int l = socketChannel.read(byteBuffer);
					if (l > 0) {

						String content = new String(byteBuffer.array(), 0, l);
						System.out.println("服务端接收到客户端的消息: " + content);
						socketChannel.register(selector, SelectionKey.OP_WRITE);
					}
				}

				if (selectionKey.isWritable()) {
					SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
					ByteBuffer byteBuffer = ByteBuffer.allocate(10240);

					byteBuffer.flip();
					byteBuffer.clear();
					String msg = "服务端向客户端发送消息: msg received!";
					byteBuffer.put(CharSetUtil.encode(msg, "UTF-8"));
					socketChannel.write(byteBuffer);
					System.out.println(msg);
//					socketChannel.register(selector, SelectionKey.OP_READ);
				}

				iterator.remove();
			}
		}
	}

	public static void main(String[] args) throws IOException {
		NIOServer nioServer = new NIOServer();
		nioServer.init();
	}
}
