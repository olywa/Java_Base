package com.zsta.test.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: shaotao.zhang
 * Date: 2018-11-22
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public class Server2 {

	public static void main(String[] args) throws IOException {
		new Server2().start();
	}

	public void start() throws IOException {
		ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(2080));

		serverSocketChannel.configureBlocking(false);

		Selector selector = Selector.open();
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

		while (selector.select() > 0) {
			Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();

			while (iterator.hasNext()) {
				SelectionKey selectionKey = iterator.next();
				if (selectionKey.isAcceptable()) {
					SocketChannel socketChannel = serverSocketChannel.accept();

					socketChannel.configureBlocking(false);

					socketChannel.register(selector, SelectionKey.OP_READ);

				} else if (selectionKey.isReadable()) {
					SocketChannel socketChannel = (SocketChannel) selectionKey.channel();

//                    socketChannel.configureBlocking(false);

					ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
					int l = 0;
					while ((l = socketChannel.read(byteBuffer)) > 0) {
						byteBuffer.flip();
						System.out.println("server: " + new String(byteBuffer.array(), 0, l));
						byteBuffer.clear();
					}
				}

				iterator.remove();
//                selector.selectedKeys().remove(selectionKey);
			}
		}
	}
}
